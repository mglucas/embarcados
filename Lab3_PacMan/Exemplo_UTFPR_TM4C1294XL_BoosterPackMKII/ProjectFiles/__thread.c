#include <stdio.h>
#include <stdlib.h>
#include "TM4C129.h" // Device header
#include <stdbool.h>
#include "grlib/grlib.h"
#include "cfaf128x128x16.h"
#include "led.h"
#include "joy.h"
#include "buzzer.h"
#include "buttons.h"
#include "driverlib/sysctl.h" // driverlib
#include <string.h>
#include "draw.h"

/*----MAPA----*/
// Treshold de cor dos pixels
#define TRESHOLD 128
#define FPS 450
#define LINHA_LENGTH 67
#define COLUNA_LENGTH 128

/*----FANTASMA----*/
// Tamanho fantasma
#define FANTASMA_LINHA_LENGTH 7
#define FANTASMA_COLUNA_LENGTH 7
#define TEMPO_BUSCA 5000
// Instancia os fantasmas
Fantasma fantasma1,fantasma2,fantasma3;
int8_t imagem_fantasma = 1;
// 0 - cima,1 - direita, 2-baixo, 3-esquerda
int8_t vetor_aleatoriedade_fantasma1[50] = {0,0,0,0,0,1,0,1,2,2,3,2,0,0,1,1,1,1,3,1,3,0,1,1,3,2,0,1,3,2,0,0,0,0,1,1,1,1,3,2,0,0,0,0,1,1,1,1,3,2}; 
int8_t vetor_aleatoriedade_fantasma2[50] = {0,0,0,0,1,1,1,1,3,2,0,1,1,0,1,1,1,1,3,2,0,0,0,0,1,1,0,1,2,2,3,2,0,0,1,1,1,1,3,2,0,0,0,0,1,1,1,1,3,2};
int8_t vetor_aleatoriedade_fantasma3[50] = {0,0,1,1,1,2,2,3,3,0,3,0,0,0,1,2,2,3,2,0,1,0,1,0,2,0,2,3,3,3,0,0,1,1,1,2,2,3,3,3,0,0,1,1,2,3,2,3,3,0};
bool fantasma_fugir = false;
int8_t pontuacao_fantasma;
	
/*----PACMAN----*/
// Tamanho pacman
#define PACMAN_LINHA_LENGTH 7
#define PACMAN_COLUNA_LENGTH 7
#define X_INICIAL_PACMAN 60
#define Y_INICIAL_PACMAN 50
#define LIMITE_PROXIMIDADE_PC_FANTASMA 20
int32_t pos_x_pacman = X_INICIAL_PACMAN;
int32_t pos_y_pacman = Y_INICIAL_PACMAN;
int32_t pos_y_pacman_morte,pos_x_pacman_morte;
int32_t pos_x_pacman_teste,pos_y_pacman_teste;
int8_t dir_joy=5,pacman_dir=5; // 0 - cima,1 - direita, 2-baixo, 3-esquerda
int32_t pos_teste; // Localiza o pacman na memoria
int8_t imagem_pacman = 0;
bool imagem_pacman_cont_subindo = false;
bool pacman_colidiu = false;
bool comeu_vitamina = false;

/*----PILULAS----*/
#define TEMPO_SOM 300
pilula Pilulas[8][16];
bool ativa_buzzer = false;
int8_t conta_pilulas = 0;

/*----VITAMINAS----*/
#define ITERA_TEMPO_VITAMINAS 20
vitamina Vitaminas[2][2];
int32_t vitamina_nao_acabou = ITERA_TEMPO_VITAMINAS;
int8_t conta_vitaminas = 0;

/*----PAINEL----*/
int16_t pontuacao = 0;
int8_t vidas = 3;
bool jogo_normal = true;
bool fim_do_jogo = false;

/*----JOYSTICK----*/
#define LIMITE_VERTICAL_JOYSTICK 90
#define LIMITE_HORIZONTAL_JOYSTICK 40
#define CIMA 0
#define DIREITA 1
#define BAIXO 2
#define ESQUERDA 3

// Variaveis timer
osTimerId frames_ID;
osTimerId Troca_busca_guarda_ID;

// Definicao dos mutex
osMutexDef(buzzer);	
osMutexId(buzzer_ID);
osMutexDef(check_vida);	
osMutexId(check_vida_ID);

// Variaveis display
tContext sContext;
// Inicializa contador de ciclos
// ---- Utilizado para seed do rand
void inicia_DWT(){
/* DWT (Data Watchpoint and Trace) registers, only exists on ARM Cortex with a DWT unit */
/*!< DWT Control register */
#define KIN1_DWT_CONTROL             (*((volatile uint32_t*)0xE0001000))

/*!< CYCCNTENA bit in DWT_CONTROL register */
#define KIN1_DWT_CYCCNTENA_BIT       (1UL<<0)

/*!< DWT Cycle Counter register */
#define KIN1_DWT_CYCCNT              (*((volatile uint32_t*)0xE0001004))

/*!< DEMCR: Debug Exception and Monitor Control Register */
#define KIN1_DEMCR                   (*((volatile uint32_t*)0xE000EDFC))

/*!< Trace enable bit in DEMCR register */
#define KIN1_TRCENA_BIT              (1UL<<24)

/*!< TRCENA: Enable trace and debug block DEMCR (Debug Exception and Monitor Control Register */	
#define KIN1_InitCycleCounter() \
	KIN1_DEMCR |= KIN1_TRCENA_BIT

/*!< Reset cycle counter */
#define KIN1_ResetCycleCounter() \
	KIN1_DWT_CYCCNT = 0

/*!< Enable cycle counter */
#define KIN1_EnableCycleCounter() \
	KIN1_DWT_CONTROL |= KIN1_DWT_CYCCNTENA_BIT

/*!< Disable cycle counter */
#define KIN1_DisableCycleCounter() \
	KIN1_DWT_CONTROL &= ~KIN1_DWT_CYCCNTENA_BIT	

/*!< Read cycle counter register */
#define KIN1_GetCycleCounter() \
	KIN1_DWT_CYCCNT
}
// Fun��o inicia perifericos	
// ---- Inicializa os perifericos
void init_all(){
	cfaf128x128x16Init();
	led_init();
	joy_init();
	button_init();
	buzzer_init();
}


// Verifica colisao fantasma
// ---- Indica a colisao pacman pilula
bool verifica_colisao_fantasma(Fantasma* fantasma){

	/* MANIPULANDO MEMORIA */
	
	// Coleta posicoes de teste
	pos_teste =(fantasma->pos_x_fantasma)+COLUNA_LENGTH*fantasma->pos_y_fantasma;

	// Se o pacman deseja ir pra cima, verifica possibilidade
	// Se for menor que o treshold, significa que estou batendo num ponto preto
	if(fantasma->fantasma_dir == 0 && (mapa[pos_teste-COLUNA_LENGTH+1] < TRESHOLD || 
											mapa[pos_teste-COLUNA_LENGTH+2] < TRESHOLD || 
											mapa[pos_teste-COLUNA_LENGTH+3] < TRESHOLD ||
											mapa[pos_teste-COLUNA_LENGTH+4] < TRESHOLD || 
											mapa[pos_teste-COLUNA_LENGTH+5] < TRESHOLD || 
											mapa[pos_teste-COLUNA_LENGTH+6] < TRESHOLD || 
											mapa[pos_teste-COLUNA_LENGTH+7] < TRESHOLD)){
		fantasma->colisao_fantasma = true;
		return false;
	}
	else
	// Testa movimento para a direita
	if(fantasma->fantasma_dir == 1 && (mapa[pos_teste+PACMAN_COLUNA_LENGTH+1] < TRESHOLD ||
											mapa[pos_teste+PACMAN_COLUNA_LENGTH+1*COLUNA_LENGTH+1] < TRESHOLD || 
											mapa[pos_teste+PACMAN_COLUNA_LENGTH+2*COLUNA_LENGTH+1] < TRESHOLD ||
											mapa[pos_teste+PACMAN_COLUNA_LENGTH+3*COLUNA_LENGTH+1] < TRESHOLD ||
											mapa[pos_teste+PACMAN_COLUNA_LENGTH+4*COLUNA_LENGTH+1] < TRESHOLD ||
											mapa[pos_teste+PACMAN_COLUNA_LENGTH+5*COLUNA_LENGTH+1] < TRESHOLD ||
											mapa[pos_teste+PACMAN_COLUNA_LENGTH+6*COLUNA_LENGTH+1] < TRESHOLD)){
		fantasma->colisao_fantasma = true;
		return false;
	}
	else
	// Testa movimento para baixo
	if(fantasma->fantasma_dir == 2 && (mapa[pos_teste+PACMAN_LINHA_LENGTH*COLUNA_LENGTH+1] < TRESHOLD || 
											mapa[pos_teste+PACMAN_LINHA_LENGTH*COLUNA_LENGTH+2] < TRESHOLD ||
											mapa[pos_teste+PACMAN_LINHA_LENGTH*COLUNA_LENGTH+3] < TRESHOLD ||
											mapa[pos_teste+PACMAN_LINHA_LENGTH*COLUNA_LENGTH+4] < TRESHOLD ||
											mapa[pos_teste+PACMAN_LINHA_LENGTH*COLUNA_LENGTH+5] < TRESHOLD ||
											mapa[pos_teste+PACMAN_LINHA_LENGTH*COLUNA_LENGTH+6] < TRESHOLD ||
											mapa[pos_teste+PACMAN_LINHA_LENGTH*COLUNA_LENGTH+7] < TRESHOLD )){
		fantasma->colisao_fantasma = true;
		return false;
	}
	else
	// Testa movimento para esquerda
	if(fantasma->fantasma_dir == 3 && (mapa[pos_teste+0*COLUNA_LENGTH] < TRESHOLD || 
											mapa[pos_teste+1*COLUNA_LENGTH] < TRESHOLD || 
											mapa[pos_teste+2*COLUNA_LENGTH] < TRESHOLD || 
											mapa[pos_teste+3*COLUNA_LENGTH] < TRESHOLD || 
											mapa[pos_teste+4*COLUNA_LENGTH] < TRESHOLD || 
											mapa[pos_teste+5*COLUNA_LENGTH] < TRESHOLD || 
											mapa[pos_teste+6*COLUNA_LENGTH] < TRESHOLD )){
		fantasma->colisao_fantasma = true;
		return false;
	}
	
		fantasma->colisao_fantasma = false;
	// Se falhar em todos os casos, confirma a possibilidade de movimentacao	
		return true;
}

// Movimenta pacman
// ---- Executa a movimentacao do pacman
void movimenta_pacman(){
			
		// Cima
		if(pacman_dir == CIMA){
			pos_y_pacman--;
		}
		else
		// Direita
		if(pacman_dir == DIREITA){
			pos_x_pacman++;
		}
		else
		// Baixo
		if(pacman_dir == BAIXO){
			pos_y_pacman++;
		}
		else
		// Esquerda
		if(pacman_dir == ESQUERDA){
			pos_x_pacman--;
		}
		else{
			pos_x_pacman_teste = pos_x_pacman;
			pos_y_pacman_teste = pos_y_pacman;
		}

}

// Verifica colisao pacman
// ---- Indica a colisao pacman paredes
bool verifica_colisao_pacman(){

	/* MANIPULANDO MEMORIA */
	
	// Coleta posicoes de teste
	pos_teste =(pos_x_pacman)+COLUNA_LENGTH*pos_y_pacman;

	// Se o pacman deseja ir pra cima, verifica possibilidade
	// Se for menor que o treshold, significa que estou batendo num ponto preto
	if(dir_joy == 0 && (mapa[pos_teste-COLUNA_LENGTH+1] < TRESHOLD || 
											mapa[pos_teste-COLUNA_LENGTH+2] < TRESHOLD || 
											mapa[pos_teste-COLUNA_LENGTH+3] < TRESHOLD ||
											mapa[pos_teste-COLUNA_LENGTH+4] < TRESHOLD || 
											mapa[pos_teste-COLUNA_LENGTH+5] < TRESHOLD || 
											mapa[pos_teste-COLUNA_LENGTH+6] < TRESHOLD || 
											mapa[pos_teste-COLUNA_LENGTH+7] < TRESHOLD)){
		dir_joy = pacman_dir;
		return false;
	}
	else
	// Testa movimento para a direita
	if(dir_joy == 1 && (mapa[pos_teste+PACMAN_COLUNA_LENGTH+1] < TRESHOLD ||
											mapa[pos_teste+PACMAN_COLUNA_LENGTH+1*COLUNA_LENGTH+1] < TRESHOLD || 
											mapa[pos_teste+PACMAN_COLUNA_LENGTH+2*COLUNA_LENGTH+1] < TRESHOLD ||
											mapa[pos_teste+PACMAN_COLUNA_LENGTH+3*COLUNA_LENGTH+1] < TRESHOLD ||
											mapa[pos_teste+PACMAN_COLUNA_LENGTH+4*COLUNA_LENGTH+1] < TRESHOLD ||
											mapa[pos_teste+PACMAN_COLUNA_LENGTH+5*COLUNA_LENGTH+1] < TRESHOLD ||
											mapa[pos_teste+PACMAN_COLUNA_LENGTH+6*COLUNA_LENGTH+1] < TRESHOLD)){
		dir_joy = pacman_dir;
		return false;
	}
	else
	// Testa movimento para baixo
	if(dir_joy == 2 && (mapa[pos_teste+PACMAN_LINHA_LENGTH*COLUNA_LENGTH+1] < TRESHOLD || 
											mapa[pos_teste+PACMAN_LINHA_LENGTH*COLUNA_LENGTH+2] < TRESHOLD ||
											mapa[pos_teste+PACMAN_LINHA_LENGTH*COLUNA_LENGTH+3] < TRESHOLD ||
											mapa[pos_teste+PACMAN_LINHA_LENGTH*COLUNA_LENGTH+4] < TRESHOLD ||
											mapa[pos_teste+PACMAN_LINHA_LENGTH*COLUNA_LENGTH+5] < TRESHOLD ||
											mapa[pos_teste+PACMAN_LINHA_LENGTH*COLUNA_LENGTH+6] < TRESHOLD ||
											mapa[pos_teste+PACMAN_LINHA_LENGTH*COLUNA_LENGTH+7] < TRESHOLD )){
		dir_joy = pacman_dir;
		return false;
	}
	else
	// Testa movimento para esquerda
	if(dir_joy == 3 && (mapa[pos_teste+0*COLUNA_LENGTH] < TRESHOLD || 
											mapa[pos_teste+1*COLUNA_LENGTH] < TRESHOLD || 
											mapa[pos_teste+2*COLUNA_LENGTH] < TRESHOLD || 
											mapa[pos_teste+3*COLUNA_LENGTH] < TRESHOLD || 
											mapa[pos_teste+4*COLUNA_LENGTH] < TRESHOLD || 
											mapa[pos_teste+5*COLUNA_LENGTH] < TRESHOLD || 
											mapa[pos_teste+6*COLUNA_LENGTH] < TRESHOLD )){
		dir_joy = pacman_dir;
		return false;
	}
	
	
	// Se falhar em todos os casos, confirma a possibilidade de movimentacao	
		pacman_dir = dir_joy;
		return true;
}


// Verifica colisao Vitamina
// ---- Indica a colisao pacman Vitamina
bool verifica_colisao_vitamina(uint32_t vitamina_x,uint32_t vitamina_y){
	
	// Colisao baixo
	if( pos_y_pacman+PACMAN_LINHA_LENGTH/2 < vitamina_y+3 && pos_y_pacman+PACMAN_LINHA_LENGTH/2 > vitamina_y+3)
		if ( pos_x_pacman+PACMAN_LINHA_LENGTH/2 > vitamina_x-2 && pos_x_pacman+PACMAN_LINHA_LENGTH/2 < vitamina_x+3)
			return true;
	
	// Colisao cima
	if( pos_y_pacman+PACMAN_LINHA_LENGTH/2 > vitamina_y && pos_y_pacman+PACMAN_LINHA_LENGTH/2 < vitamina_y+3)
		if ( pos_x_pacman+PACMAN_LINHA_LENGTH/2 > vitamina_x-2 && pos_x_pacman+PACMAN_LINHA_LENGTH/2 < vitamina_x+3)
			return true;	
	
	// Em caso de n�o encontrar
	return false;

}

// Verifica colisao pilula
// ---- Indica a colisao pacman pilula
bool verifica_colisao_pilula(uint32_t pilula_x,uint32_t pilula_y){
	
	// Colisao vertical
	if(pilula_y > pos_y_pacman && pilula_y < pos_y_pacman+PACMAN_LINHA_LENGTH){
		if (pilula_x > pos_x_pacman && pilula_x < pos_x_pacman+PACMAN_COLUNA_LENGTH)
			return true;
	}
	else
	// Colisao horizontal
	if (pilula_x > pos_x_pacman && pilula_x < pos_x_pacman+PACMAN_COLUNA_LENGTH){	
		if(pilula_y > pos_y_pacman && pilula_y < pos_y_pacman+PACMAN_LINHA_LENGTH)
			return true;
	}
	else
		
	// Em caso de n�o encontrar
	return false;

}



// Movimenta fantasma
// ---- Executa a movimentacao do fantasma
void movimenta_fantasma(Fantasma* fantasma){
			
		// Cima
		if(fantasma->fantasma_dir == CIMA){
			fantasma->pos_y_fantasma-=1;
		}
		else
		// Direita
		if(fantasma->fantasma_dir == DIREITA){
			fantasma->pos_x_fantasma+=1;
		}
		else
		// Baixo
		if(fantasma->fantasma_dir == BAIXO){
			fantasma->pos_y_fantasma+=1;
		}
		else
		// Esquerda
		if(fantasma->fantasma_dir == ESQUERDA){
			fantasma->pos_x_fantasma-=1;
		}
		else{
			fantasma->pos_x_fantasma_teste = fantasma->pos_x_fantasma;
			fantasma->pos_y_fantasma_teste = fantasma->pos_y_fantasma;
		}

}
	
 
// Cacada ao pacman
// ---- Indica a movimentacao necessaria para alcancar o pacman
void cacada_pacman(Fantasma* fantasma){

	// Seguir na horizontal
	if(pos_x_pacman > fantasma->pos_x_fantasma){
		fantasma->fantasma_dir = DIREITA;
		if(verifica_colisao_fantasma(fantasma))
			return;
	}

	if(pos_x_pacman < fantasma->pos_x_fantasma){
		fantasma->fantasma_dir = ESQUERDA;
		if(verifica_colisao_fantasma(fantasma))
			return;
	}
	
	// Seguir na vertical
	if(pos_y_pacman > fantasma->pos_y_fantasma){
		fantasma->fantasma_dir = BAIXO;
		if(verifica_colisao_fantasma(fantasma))
			return;
	}
	
	if(pos_y_pacman < fantasma->pos_y_fantasma){
		fantasma->fantasma_dir = CIMA;
		if(verifica_colisao_fantasma(fantasma))
			return;
	}

}
// Cacada ao pacman
// ---- Indica a movimentacao necessaria para alcancar o pacman
void fuga_pacman(Fantasma* fantasma){

	// Seguir na horizontal
	if(pos_x_pacman > fantasma->pos_x_fantasma){
		fantasma->fantasma_dir = ESQUERDA;
		if(verifica_colisao_fantasma(fantasma))
			return;
	}

	if(pos_x_pacman < fantasma->pos_x_fantasma){
		fantasma->fantasma_dir = DIREITA;
		if(verifica_colisao_fantasma(fantasma))
			return;
	}
	
	// Seguir na vertical
	if(pos_y_pacman > fantasma->pos_y_fantasma){
		fantasma->fantasma_dir = CIMA;
		if(verifica_colisao_fantasma(fantasma))
			return;
	}
	
	if(pos_y_pacman < fantasma->pos_y_fantasma){
		fantasma->fantasma_dir = BAIXO;
		if(verifica_colisao_fantasma(fantasma))
			return;
	}

}
// Cacada ao pacman PREVISAO
// ---- Indica a movimentacao necessaria para alcancar uma possivel futura posicao do pacman
bool proximidade_pacman(Fantasma* fantasma){

	// Se o pacman esta proximo em altura 
	if(abs(pos_y_pacman - fantasma->pos_y_fantasma) < LIMITE_PROXIMIDADE_PC_FANTASMA)
		return false;
	else
	if(abs(pos_x_pacman - fantasma->pos_x_fantasma) < LIMITE_PROXIMIDADE_PC_FANTASMA)
		return false;
	else
		return true;
}

// Checa colisao pacman-fantasma
// ---- Indica colisao apos realizadas as movimentacoes
bool colisao_fantasma_pacman(Fantasma fantasma){

	// Colisao vertical - Fantasma embaixo ou em cima pacman
	// 1 VErificacao - Pacman mesma coluna que o fantasma
	// 2 Verificacao - Fantasma passou pelo pacman
	if((pos_x_pacman + PACMAN_LINHA_LENGTH/2 > fantasma.pos_x_fantasma && pos_x_pacman + PACMAN_LINHA_LENGTH/2 < fantasma.pos_x_fantasma + FANTASMA_COLUNA_LENGTH) &&
		 (pos_y_pacman + PACMAN_LINHA_LENGTH > fantasma.pos_y_fantasma &&  pos_y_pacman < fantasma.pos_y_fantasma + FANTASMA_LINHA_LENGTH)){
		return true;
	}
	else
	// Colisao horizontal - Fantasma do lado do pacman
	if((pos_y_pacman + PACMAN_COLUNA_LENGTH/2 >= fantasma.pos_y_fantasma &&	pos_y_pacman + PACMAN_COLUNA_LENGTH/2 <= fantasma.pos_y_fantasma + FANTASMA_LINHA_LENGTH) &&
			(pos_x_pacman + PACMAN_COLUNA_LENGTH/2 > fantasma.pos_x_fantasma && pos_x_pacman + PACMAN_COLUNA_LENGTH/2 <= fantasma.pos_x_fantasma + FANTASMA_LINHA_LENGTH)){
		return true;
	}

	return false;
}


// Inicia Variaveis
// ---- Inicia e reinicia variaveis dos personagens
void inicia_variaveis(){
	uint8_t index;
	
	// Pacman
	pos_x_pacman = X_INICIAL_PACMAN;
	pos_y_pacman = Y_INICIAL_PACMAN;
	dir_joy=5,pacman_dir=5; // 0 - cima,1 - direita, 2-baixo, 3-esquerda

	// Fantasma
	// Inicia os fantasmas
	//Fantasma1
	fantasma1.pos_x_fantasma = X_INICIAL_FANTASMA;
	fantasma1.pos_y_fantasma = Y_INICIAL_FANTASMA;
	//Fantasma2
	fantasma2.pos_x_fantasma = X_INICIAL_FANTASMA;
	fantasma2.pos_y_fantasma = Y_INICIAL_FANTASMA;
	//Fantasma3
	fantasma3.pos_x_fantasma = X_INICIAL_FANTASMA;
	fantasma3.pos_y_fantasma = Y_INICIAL_FANTASMA;
	
	// Inicializa variaveis de set de busca
	fantasma1.busca = true;
	fantasma2.busca = true;
	fantasma3.busca = true;
	
	// Inicia todos como vivo
	fantasma1.vivo = true;
	fantasma2.vivo = true;
	fantasma3.vivo = true;
	
	// Colocar direcoes aleatorias
	fantasma1.fantasma_dir = rand() % 3;
	fantasma2.fantasma_dir = rand() % 3;
	fantasma3.fantasma_dir = rand() % 3;

	// Pontuacao incremental
	pontuacao_fantasma = 20;
}
// Reinicia Variaveis
// ---- Reinicia variaveis do JOGO
void reinicia_variaveis(){	
	vitamina_nao_acabou = ITERA_TEMPO_VITAMINAS;
	
	conta_vitaminas = 0;
	conta_pilulas = 0;
	
	// Pacman
	pos_x_pacman = X_INICIAL_PACMAN;
	pos_y_pacman = Y_INICIAL_PACMAN;
	dir_joy=5,pacman_dir=5; // 0 - cima,1 - direita, 2-baixo, 3-esquerda

	// Fantasma
	// Inicia os fantasmas
	//Fantasma1
	fantasma1.pos_x_fantasma = X_INICIAL_FANTASMA;
	fantasma1.pos_y_fantasma = Y_INICIAL_FANTASMA;
	//Fantasma2
	fantasma2.pos_x_fantasma = X_INICIAL_FANTASMA;
	fantasma2.pos_y_fantasma = Y_INICIAL_FANTASMA;
	//Fantasma3
	fantasma3.pos_x_fantasma = X_INICIAL_FANTASMA;
	fantasma3.pos_y_fantasma = Y_INICIAL_FANTASMA;
	
	// Inicializa variaveis de set de busca
	fantasma1.busca = true;
	fantasma2.busca = true;
	fantasma3.busca = true;
	
	// Inicia todos como vivo
	fantasma1.vivo = true;
	fantasma2.vivo = true;
	fantasma3.vivo = true;
	
	// Colocar direcoes aleatorias
	fantasma1.fantasma_dir = rand() % 3;
	fantasma2.fantasma_dir = rand() % 3;
	fantasma3.fantasma_dir = rand() % 3;

	// Pontuacao incremental
	pontuacao_fantasma = 20;
	
	pacman_colidiu = false;
	jogo_normal = true;
	fim_do_jogo = false;
	vidas = 3;
	pontuacao = 0;
	
	// Inicia pilulas e vitaminas
	Inicia_pilulas(Pilulas);
	Inicia_vitamina(Vitaminas);

	limpa_tela(sContext);

}

// Volta ao inicio
// ---- Quando o fantasma morre, volta ao inicio
void retorno_fantasma(Fantasma* fantasma){
	
	// Seguir na horizontal
	if(X_INICIAL_FANTASMA > fantasma->pos_x_fantasma){
		fantasma->fantasma_dir = DIREITA;
		if(verifica_colisao_fantasma(fantasma))
			return;
	}

	if(X_INICIAL_FANTASMA < fantasma->pos_x_fantasma){
		fantasma->fantasma_dir = ESQUERDA;
		if(verifica_colisao_fantasma(fantasma))
			return;
	}
	
	// Seguir na vertical
	if(Y_INICIAL_FANTASMA > fantasma->pos_y_fantasma){
		fantasma->fantasma_dir = BAIXO;
		if(verifica_colisao_fantasma(fantasma))
			return;
	}
	
	if(Y_INICIAL_FANTASMA< fantasma->pos_y_fantasma){
		fantasma->fantasma_dir = CIMA;
		if(verifica_colisao_fantasma(fantasma))
			return;
	}

	
	if( (fantasma->pos_x_fantasma+FANTASMA_COLUNA_LENGTH/2 > X_INICIAL_FANTASMA && 
		fantasma->pos_x_fantasma + PACMAN_COLUNA_LENGTH/2 < X_INICIAL_FANTASMA + PACMAN_COLUNA_LENGTH) ||
		(fantasma->pos_y_fantasma + PACMAN_LINHA_LENGTH/2 > Y_INICIAL_FANTASMA &&
		fantasma->pos_y_fantasma + PACMAN_LINHA_LENGTH/2 < Y_INICIAL_FANTASMA+PACMAN_LINHA_LENGTH)){
		fantasma->vivo = true;
		fantasma->fugir = false;
		return;
	}
}
// Musica inicial
// ---- Toca musica inicial
void musica_inicial(){
	#ifndef GANTT
	buzzer_vol_set(5000);
	buzzer_per_set(3000);
	buzzer_write(true);
	osDelay(2500);
	buzzer_write(false);
	buzzer_per_set(2000);
	buzzer_write(true);
	osDelay(1500);
	buzzer_write(false);
	buzzer_per_set(1500);
	buzzer_write(true);
	osDelay(1000);
	buzzer_write(false);
	osDelay(1000);
	#endif
}
// Musica final
// ---- Toca musica final
void musica_final(){
	#ifndef GANTT
	buzzer_vol_set(5000);
	buzzer_per_set(1500);
	buzzer_write(true);
	osDelay(2500);
	buzzer_write(false);
	buzzer_per_set(2000);
	buzzer_write(true);
	osDelay(1500);
	buzzer_write(false);
	buzzer_per_set(3000);
	buzzer_write(true);
	osDelay(1000);
	buzzer_write(false);
	osDelay(1000);
	#endif
}
// Cria��o das threads 
void Menu_inicial(void const *argument);
void Desenho_inicial(void const *argument);
void Gerencia_botoes(void const *argument);
void Fantasmas(void const *argument);
void Pacman(void const *argument);
void Gerencia_pilulas(void const *argument);
void Gerencia_vitaminas(void const *argument);
void Painel_instrumentos(void const *argument);
void Atualiza_desenho(void const *argument);

// Vari�vel que determina ID das threads
osThreadId Menu_inicial_ID; 
osThreadId Desenho_inicial_ID; 
osThreadId Gerencia_botoes_ID; 
osThreadId Fantasmas_ID; 
osThreadId Pacman_ID;
osThreadId Gerencia_pilulas_ID; 
osThreadId Gerencia_vitaminas_ID; 
osThreadId Painel_instrumentos_ID; 
osThreadId Atualiza_desenho_ID; 

// Defini��o das threads
osThreadDef (Menu_inicial, osPriorityNormal, 1, 0);     // thread object
osThreadDef (Desenho_inicial, osPriorityNormal, 1, 0);     // thread object
osThreadDef (Gerencia_botoes, osPriorityNormal, 1, 0);     // thread object
osThreadDef (Fantasmas, osPriorityNormal, 1, 0);     // thread object
osThreadDef (Pacman, osPriorityNormal, 1, 0);     // thread object
osThreadDef (Gerencia_pilulas, osPriorityNormal, 1, 0);     // thread object
osThreadDef (Gerencia_vitaminas, osPriorityNormal, 1, 0);     // thread object
osThreadDef (Painel_instrumentos, osPriorityNormal, 1, 0);     // thread object
osThreadDef (Atualiza_desenho, osPriorityNormal, 1, 0);     // thread object

// Inicializa as threads
int Init_Thread (void) {
	
	Menu_inicial_ID = osThreadCreate (osThread(Menu_inicial), NULL);
	if (!Menu_inicial_ID) return(-1);
	
	Desenho_inicial_ID = osThreadCreate (osThread(Desenho_inicial), NULL);
	if (!Desenho_inicial_ID) return(-1);
	
	Gerencia_botoes_ID = osThreadCreate (osThread(Gerencia_botoes), NULL);
	if (!Gerencia_botoes_ID) return(-1);
	
	Fantasmas_ID = osThreadCreate (osThread(Fantasmas), NULL);
	if (!Fantasmas_ID) return(-1);
	
	Pacman_ID = osThreadCreate (osThread(Pacman), NULL); 
	if (!Pacman_ID) return(-1);
	
	Gerencia_pilulas_ID = osThreadCreate (osThread(Gerencia_pilulas), NULL); 
	if (!Gerencia_pilulas_ID) return(-1);
	
	Gerencia_vitaminas_ID = osThreadCreate (osThread(Gerencia_vitaminas), NULL); 
	if (!Gerencia_vitaminas_ID) return(-1);
	
	Painel_instrumentos_ID = osThreadCreate (osThread(Painel_instrumentos), NULL); 
	if (!Painel_instrumentos_ID) return(-1);
 
	Atualiza_desenho_ID = osThreadCreate (osThread(Atualiza_desenho), NULL); 
	if (!Atualiza_desenho_ID) return(-1);
	
	return(0);
}



// Callback do timer
// Callback do timer de atualizacao da tela
void Timer_fps (void const *argument){
	osSignalSet(Gerencia_botoes_ID,0x01);
	osSignalSet(Fantasmas_ID,0x01);	
}
// Callback do timer de altern�ncia do estado do fantasma
void Timer_acao_fantasma (void const *argument){
	fantasma1.busca = !fantasma1.busca;
	
	vitamina_nao_acabou--;
	if(vitamina_nao_acabou == 0){
		comeu_vitamina = false;
		fantasma1.fugir = false;
		fantasma2.fugir = false;
		fantasma3.fugir = false;
		fantasma1.vivo = true;
		fantasma2.vivo = true;
		fantasma3.vivo = true;
		pontuacao_fantasma = 20;
		vitamina_nao_acabou = 20;
	}
	
	// Decide qual imagem sera mostrada na tela
	// Verifica qual imagem printar
	if(imagem_fantasma < 4){
		imagem_fantasma++;
	}
	else
		imagem_fantasma = 1;
}
/*-----THREADS-----*/
// Thread que gear os n�meros  
void Menu_inicial (void const *argument) {
	uint32_t cycles; // Numero de ciclos ate o momento
	bool button = false;
	
	while(true){
		osSignalWait(0x01,osWaitForever);
		osSignalClear(Menu_inicial_ID,0x01);
		
		osTimerStart(Troca_busca_guarda_ID,TEMPO_BUSCA);
		desenho_mapa(sContext);
		escreve_frase(sContext);
		inicia_variaveis();
		pontuacao = 0;
		
		#ifndef GANTT
		// Enquanto nao pressionar o botao
		while(button_read_s1() == false);

		desenho_mapa(sContext);
		
		// Toca musica inicial
		musica_inicial();
		
		// Conta ciclos para a srand ficar aleatoria
		cycles = KIN1_GetCycleCounter(); /* get cycle counter */
		KIN1_DisableCycleCounter(); /* disable counting if not used any more */
		#endif
		// Ajusta a Srand
		srand(cycles);
		
		// MENU 
		// START GAME
		// RESET 
		
		
		// Passa o controle para proxima tarefa
		osSignalSet(Desenho_inicial_ID,0x01);
	}	
}
// Thread que verifica os primos
void Desenho_inicial (void const *argument){

	while(true){
		// Espera a habilita��o do menu inicial
		osSignalWait(0x01,osWaitForever);
		osSignalClear(Desenho_inicial_ID,0x01);

		desenho_pilulas(sContext,Pilulas);
		desenho_vitaminas(sContext,Vitaminas);
		desenho_painel(sContext,pontuacao,vidas);
	

		// TESTANDO MOVIMENTACAO
		osTimerStart(frames_ID,FPS);
		
	}
}

// Thread que decodifica a mensagem
void Gerencia_botoes(void const *argument) {
	int16_t joy_x;
	int16_t joy_y;
	
	while(true){

		// Espera a habilita��o do timer de fps
		osSignalWait(0x01,osWaitForever);
		osSignalClear(Gerencia_botoes_ID,0x01);
		
		// Lendo os valores de x e y
		#ifndef GANTT
		joy_x = joy_read_x();
		joy_y = joy_read_y();
		#endif
		
		// Adequando pra um intervalo especifico de medida
		joy_x = joy_x*200/0xFFF-100;
		joy_y = joy_y*200/0xFFF-100;
		
		// joystik cima
		if ( joy_y > LIMITE_VERTICAL_JOYSTICK & joy_x > -LIMITE_HORIZONTAL_JOYSTICK & joy_x < LIMITE_HORIZONTAL_JOYSTICK){
			pos_y_pacman_teste = pos_y_pacman - 1;
			pos_x_pacman_teste = pos_x_pacman;
			dir_joy = CIMA;
		}								
		else
		// joystick baixo
		if ( joy_y < -LIMITE_VERTICAL_JOYSTICK & joy_x > -LIMITE_HORIZONTAL_JOYSTICK & joy_x < LIMITE_HORIZONTAL_JOYSTICK){
			pos_y_pacman_teste = pos_y_pacman + 1;
			pos_x_pacman_teste = pos_x_pacman;
			dir_joy = BAIXO;
		}									
		else
		// joystick esquerda
		if ( joy_x < -LIMITE_VERTICAL_JOYSTICK & joy_y > -LIMITE_HORIZONTAL_JOYSTICK & joy_y < LIMITE_HORIZONTAL_JOYSTICK){
			pos_y_pacman_teste = pos_y_pacman;
			pos_x_pacman_teste = pos_x_pacman - 1;
			dir_joy = ESQUERDA;
		}	
		else
		// joystick direita
		if ( joy_x > LIMITE_VERTICAL_JOYSTICK & joy_y > -LIMITE_HORIZONTAL_JOYSTICK & joy_y < LIMITE_HORIZONTAL_JOYSTICK){
			pos_y_pacman_teste = pos_y_pacman;
			pos_x_pacman_teste = pos_x_pacman + 1;		
			dir_joy = DIREITA;
		}
		else{
			// Continua movimento para cima
			if(dir_joy == 0){
				pos_y_pacman_teste = pos_y_pacman - 1 ;
				pos_x_pacman_teste = pos_x_pacman;						
			}
			// Continua movimento para direita
			else
			if(dir_joy == 1){
				pos_y_pacman_teste = pos_y_pacman;
				pos_x_pacman_teste = pos_x_pacman + 1;
			}
			// Continua movimento para baixo
			else
			if(dir_joy == 2){
				pos_y_pacman_teste = pos_y_pacman + 1;
				pos_x_pacman_teste = pos_x_pacman;
			}
			// Continua movimento para esquerda
			else
			if(dir_joy == 3){
				pos_y_pacman_teste = pos_y_pacman;
				pos_x_pacman_teste = pos_x_pacman - 1;
			}
			// Mantem parado no inicio do jogo
			else
				pos_x_pacman_teste = pos_x_pacman;
				pos_y_pacman_teste = pos_y_pacman;
		}
		
		// Seta o flag do pacman, para verificar possibilidade de movimentacao
		osSignalSet(Pacman_ID,0x01);
	}
}
	
// Thread que verifica o penultimo valor
void Fantasmas(void const *argument) {
	// Inicia o timer de busca dos fantasmas
	osTimerStart(Troca_busca_guarda_ID,TEMPO_BUSCA);
	
	while(true){
		// Espera a habilita��o do timer de fps
		osSignalWait(0x01,osWaitForever);
		osSignalClear(Fantasmas_ID,0x01);
		
		if(fantasma1.fugir == false){
			/* FANTASMA1 - Ca�a o pacman na sua exata posicao*/
			// Se esta buscando, usa funcao de caca ao pacman
			if(fantasma1.busca){
				// buscar
				// Verifica o necessario para cacar o pacman
				cacada_pacman(&fantasma1);
				// Verifica se e possivel seguir a ordem 
				if(verifica_colisao_fantasma(&fantasma1))
				// Se for possivel, executa o movimento.
					movimenta_fantasma(&fantasma1);
			}
			else{
				// scatter - Caminha pelo mapa aleatoriamente
				// Verifica se e possivel seguir a ordem 
				if(verifica_colisao_fantasma(&fantasma1))
				// Se for possivel, executa o movimento.
					movimenta_fantasma(&fantasma1);
				else
					fantasma1.fantasma_dir = vetor_aleatoriedade_fantasma1[rand() % 49];
			}
		}
		else{
			// FANTASMA 1 - fugindo
			// Em relacao a posi�ao atual, verifica para onde deve fugir
			if(fantasma1.vivo == true){
				fuga_pacman(&fantasma1);
				// Executo o movimento pensado
				if(verifica_colisao_fantasma(&fantasma1))
				// Se for possivel, executa o movimento.
					movimenta_fantasma(&fantasma1);
			}
			else{
				retorno_fantasma(&fantasma1);
				// Executo o movimento pensado
				if(verifica_colisao_fantasma(&fantasma1))
				// Se for possivel, executa o movimento.
					movimenta_fantasma(&fantasma1);				
				else{
				fantasma1.fantasma_dir = vetor_aleatoriedade_fantasma1[rand() % 49];
				if(verifica_colisao_fantasma(&fantasma1))
					movimenta_fantasma(&fantasma1);
				}
			}		
		}
			
		if(fantasma2.fugir == false){
			/* FANTASMA2 - Nao se aproxima o maximo do pacman*/
			// Se esta buscando, usa funcao de caca ao pacman
			if(proximidade_pacman(&fantasma2) && fantasma2.busca == true){
				// buscar
				// Verifica o necessario para cacar o pacman
				cacada_pacman(&fantasma2);
				// Verifica se e possivel seguir a ordem 
				if(verifica_colisao_fantasma(&fantasma2))
				// Se for possivel, executa o movimento.
					movimenta_fantasma(&fantasma2);
			}
			else{
				// scatter - Caminha pelo mapa aleatoriamente
				// Verifica se e possivel seguir a ordem 
				if(verifica_colisao_fantasma(&fantasma2))
				// Se for possivel, executa o movimento.
					movimenta_fantasma(&fantasma2);
				else
					fantasma2.fantasma_dir = vetor_aleatoriedade_fantasma2[rand() % 49];
			}
		}
		else{
			// FANTASMA 2 - fugindo
			// Em relacao a posi�ao atual, verifica para onde deve fugir
			if(fantasma2.vivo == true){
				fuga_pacman(&fantasma2);
				// Executo o movimento pensado
				if(verifica_colisao_fantasma(&fantasma2))
				// Se for possivel, executa o movimento.
					movimenta_fantasma(&fantasma2);
			}
			else{
				retorno_fantasma(&fantasma2);
				// Executo o movimento pensado
				if(verifica_colisao_fantasma(&fantasma2))
				// Se for possivel, executa o movimento.
					movimenta_fantasma(&fantasma2);
				else{
				fantasma2.fantasma_dir = vetor_aleatoriedade_fantasma2[rand() % 49];
				if(verifica_colisao_fantasma(&fantasma2))
					movimenta_fantasma(&fantasma2);
				}
			}				
		}
			
		if(fantasma3.fugir == false){
			/* FANTASMA3  -  Anda ate o pacman, mas para se chegar mto perto*/
			// scatter - Caminha pelo mapa aleatoriamente
			// Verifica se e possivel seguir a ordem 
			if(verifica_colisao_fantasma(&fantasma3))
			// Se for possivel, executa o movimento.
				movimenta_fantasma(&fantasma3);
			else
				fantasma3.fantasma_dir = vetor_aleatoriedade_fantasma3[rand() % 49];				
		}
		else{
			/* FANTASMA3  -  Anda ate o pacman, mas para se chegar mto perto*/
			// Se esta buscando, usa funcao de caca ao pacman
			if(fantasma3.vivo == true){
				// scatter - Caminha pelo mapa aleatoriamente
				// Verifica se e possivel seguir a ordem 
				if(verifica_colisao_fantasma(&fantasma3))
				// Se for possivel, executa o movimento.
					movimenta_fantasma(&fantasma3);
				else
					fantasma3.fantasma_dir = vetor_aleatoriedade_fantasma3[rand() % 49];
			}
			else{
				retorno_fantasma(&fantasma3);
				// Executo o movimento pensado
				if(verifica_colisao_fantasma(&fantasma3))
				// Se for possivel, executa o movimento.
					movimenta_fantasma(&fantasma3);
				else{
				fantasma3.fantasma_dir = vetor_aleatoriedade_fantasma3[rand() % 49];
				if(verifica_colisao_fantasma(&fantasma3))
					movimenta_fantasma(&fantasma3);
				}
			}				
		}		

		// Seta o flag do painel
		osSignalSet(Painel_instrumentos_ID,0x04);
	}
}

// Thread que verifica o ultimo valor
void Pacman(void const *argument) {
	while(true){
		// Espera a habilita��o do timer de fps
		osSignalWait(0x01,osWaitForever);
		osSignalClear(Pacman_ID,0x01);
		
		// Decide qual imagem sera mostrada na tela
		// Verifica qual imagem printar
		if(imagem_pacman <= 3 & imagem_pacman_cont_subindo == true ){
			imagem_pacman_cont_subindo = true;
			imagem_pacman++;
		}
		else
			imagem_pacman_cont_subindo = false;
			
		if(imagem_pacman >= 0 & imagem_pacman_cont_subindo == false){ 
			imagem_pacman_cont_subindo = false;
			imagem_pacman--;
		}
		else
			imagem_pacman_cont_subindo = true;
		
		// Posicao inicial do pacman
		// Se o teste de colisao der verdadeiro, quer dizer que nao ha colisao
		// Deste modo, podemos trocar as variaveis
		if(verifica_colisao_pacman()){
			movimenta_pacman();
		}
		// mantem as variaveis em caso de reprovacao do teste de colisao
			
		// Checa por colisao do pacman com  o fantasma
		// Sem a habilitacao da vitamina

		if(jogo_normal == true && comeu_vitamina == false && (colisao_fantasma_pacman(fantasma1) || colisao_fantasma_pacman(fantasma2) || colisao_fantasma_pacman(fantasma3))){
			pacman_colidiu = true;
			//**************************
			// ABERTURA DE SECAO CRITICA
			//**************************
			osMutexWait(check_vida_ID,osWaitForever);
			vidas--;
			osMutexRelease(check_vida_ID);
			//**************************
			// FECHAMENTO DE SECAO CRITICA
			//**************************
		}
		// Com a habilitacao da vitamina
		else
		if(jogo_normal == true && comeu_vitamina == true){ 
			if(colisao_fantasma_pacman(fantasma1) && fantasma1.vivo == true){
				pontuacao += pontuacao_fantasma;
				pontuacao_fantasma *= 2;
				fantasma1.vivo = false;
			}
			if(colisao_fantasma_pacman(fantasma2) && fantasma2.vivo == true){
				pontuacao += pontuacao_fantasma;
				pontuacao_fantasma *= 2;
				fantasma2.vivo = false;
			}
			if(colisao_fantasma_pacman(fantasma3) && fantasma3.vivo == true){
				pontuacao += pontuacao_fantasma;
				pontuacao_fantasma *= 2;
				fantasma3.vivo = false;			
			}
			
		}
	

		
		// Checa se o pacman ultrapassou o limite da parede - Inferior e superior
		if( pos_x_pacman >= 60 && pos_x_pacman + PACMAN_COLUNA_LENGTH <= 68){
			if( pos_y_pacman < 2 ){
				// Sai na parte de baixo 
				limpar_desenho_pacman(sContext,pos_x_pacman,pos_y_pacman);
				pos_y_pacman = LINHA_LENGTH - PACMAN_LINHA_LENGTH - 1;
			} 
			else
			if( pos_y_pacman + PACMAN_LINHA_LENGTH  > 65){
				// Sai na parte de cima
				limpar_desenho_pacman(sContext,pos_x_pacman,pos_y_pacman);
				pos_y_pacman = 2;
			}
		}
		
		
		// Seta o flag das gerencias
		osSignalSet(Gerencia_vitaminas_ID,0x01);
		osSignalSet(Gerencia_pilulas_ID,0x01);
	}
}
// Thread que mostra o resultado no oled
void Gerencia_pilulas(void const *argument) {
	uint32_t linha,coluna;
	
	while(true){
		// Espera a habilita��o do timer de fps
		osSignalWait(0x01,osWaitForever);
		osSignalClear(Gerencia_pilulas_ID,0x01);
	
		// Divide a posicao por 8 para saber exatamente a pilula escolhida
		linha = pos_y_pacman/8;	
						

		#ifndef GANTT
		// Vari�veis do buzzer para essa thread
		buzzer_per_set(1500);
		#endif

		for(coluna = 0; coluna < 16;coluna++){
			if(Pilulas[linha][coluna].ativo == 1 && verifica_colisao_pilula(Pilulas[linha][coluna].coluna,pos_y_pacman)){
				Pilulas[linha][coluna].ativo = 0;
				// PONTUACAO
				//**************************
				// ABERTURA DE SECAO CRITICA
				//**************************				
				osMutexWait(check_vida_ID,osWaitForever);
				pontuacao+=1;
				conta_pilulas+=1;
				osMutexRelease(check_vida_ID);
				//**************************
				// FECHAMENTO DE SECAO CRITICA
				//**************************
				//Estrutura do buzzer
				#ifndef GANTT
				osMutexWait(buzzer_ID,osWaitForever);
				buzzer_write(true);
				osDelay(TEMPO_SOM);
				buzzer_write(false);
				osMutexRelease(buzzer_ID);
				#endif
			}
	 }		
	
		// Seta para a proxima thread
		osSignalSet(Painel_instrumentos_ID,0x01);
	}
}
// Thread que mostra o resultado no oled
void Gerencia_vitaminas(void const *argument) {
	uint32_t linha,coluna;

	while(true){
		// Espera a habilita��o do timer de fps
		osSignalWait(0x01,osWaitForever);
		osSignalClear(Gerencia_pilulas_ID,0x01);

		#ifndef GANTT
		// Set das configs do buzzer
		buzzer_per_set(3000);
		#endif
		
		for(linha = 0;linha<2;linha++){
       for(coluna = 0; coluna < 2;coluna++){
					if(Vitaminas[linha][coluna].ativo == 1 && verifica_colisao_vitamina(Vitaminas[linha][coluna].coluna,Vitaminas[linha][coluna].linha)){
						Vitaminas[linha][coluna].ativo = 0;
						// PONTUACAO
						//**************************
						// ABERTURA DE SECAO CRITICA
						//**************************
						osMutexWait(check_vida_ID,osWaitForever);
						conta_vitaminas +=1;
						pontuacao+=2;	
						osMutexRelease(check_vida_ID);
						//**************************
						// FECHAMENTO DE SECAO CRITICA
						//**************************
						//Comeu vitamina
						comeu_vitamina = true;
						fantasma1.fugir = true;
						fantasma2.fugir = true;
						fantasma3.fugir = true;
						//Estrutura do buzzer
						#ifndef GANTT
						buzzer_write(true);
						osDelay(TEMPO_SOM);
						buzzer_write(false);
						#endif
					}		
			 }
		}
		// Seta para a proxima thread
		osSignalSet(Painel_instrumentos_ID,0x02);
	}
}
// Thread que finaliza e mostra o tempo de execucao
void Painel_instrumentos(void const *argument) {
	while(true){	
		// Espera a habilita��o do timer de fps
		osSignalWait(0x07,osWaitForever);
		osSignalClear(Painel_instrumentos_ID,0x07);

		// Verifica a quantidade de vidas do pacman e decide se reinicia o jogo ou nao
		if(pacman_colidiu == true){
			// Reajusta a flag
			pacman_colidiu = false;
			// Verifica numero de vidas
			if(vidas != 0){
				// Animacao morte
				jogo_normal = false;
				// Reinicia a partida
			}
			else{
				fim_do_jogo = true;
				// Volta para o menu inicial
				// Reset de vari�veis
			}
		}
		
		if(conta_pilulas == 126 && conta_vitaminas == 4){
			jogo_normal = false;
			fim_do_jogo = true;
		}

		// Seta o sinal do atualiza desenho
		osSignalSet(Atualiza_desenho_ID,0x01);
	}
}
// Thread que finaliza e mostra o tempo de execucao
void Atualiza_desenho(void const *argument) {
	// Guarda posicao atual pacman
	int32_t pos_x_pacman_morte,pos_y_pacman_morte;
	
	while(true){
		// Espera a habilita��o do painel de instrumentos
		osSignalWait(0x01,osWaitForever);
		osSignalClear(Atualiza_desenho_ID,0x01);
		
		pos_x_pacman_morte = pos_x_pacman;
		pos_y_pacman_morte = pos_y_pacman;
		
		// Se o jogo continua normal
		if(jogo_normal == true){
			desenho_pilulas(sContext,Pilulas);
			desenho_vitaminas(sContext,Vitaminas);
			desenho_painel(sContext,pontuacao,vidas);
			if(comeu_vitamina == false){
				desenho_fantasma(sContext,&fantasma1,COR_FANTASMA1,imagem_fantasma,fantasma1.colisao_fantasma);
				desenho_fantasma(sContext,&fantasma2,COR_FANTASMA2,imagem_fantasma,fantasma1.colisao_fantasma);
				desenho_fantasma(sContext,&fantasma3,COR_FANTASMA3,imagem_fantasma,fantasma1.colisao_fantasma);
			}
			else{
				if(fantasma1.vivo == true)
					desenho_fantasma(sContext,&fantasma1,COR_FUGA,imagem_fantasma,fantasma1.colisao_fantasma);
				else
					desenho_fantasma(sContext,&fantasma1,COR_FUGA,5,fantasma1.colisao_fantasma);
				
				if(fantasma2.vivo == true)
					desenho_fantasma(sContext,&fantasma2,COR_FUGA,imagem_fantasma,fantasma1.colisao_fantasma);
				else
					desenho_fantasma(sContext,&fantasma2,COR_FUGA,5,fantasma1.colisao_fantasma);
				
				if(fantasma3.vivo == true)
					desenho_fantasma(sContext,&fantasma3,COR_FUGA,imagem_fantasma,fantasma1.colisao_fantasma);
				else
					desenho_fantasma(sContext,&fantasma3,COR_FUGA,5,fantasma1.colisao_fantasma);
			}	
			desenho_pacman(sContext,pos_x_pacman,pos_y_pacman,pacman_dir,imagem_pacman);
		}
		else
		if(jogo_normal == false && fim_do_jogo == false)
		{
			// Animacao pacman morrendo
			limpar_desenho_pacman(sContext,pos_x_pacman,pos_y_pacman);
			desenho_pacman_morrendo1(sContext,pos_x_pacman_morte,pos_y_pacman_morte);
			osDelay(1000);
			desenho_pacman_morrendo2(sContext,pos_x_pacman_morte,pos_y_pacman_morte);
			osDelay(1000);
			desenho_pacman_morrendo3(sContext,pos_x_pacman_morte,pos_y_pacman_morte);
			osDelay(1000);
			desenho_pacman_morrendo4(sContext,pos_x_pacman_morte,pos_y_pacman_morte);
			osDelay(1000);									
			desenho_painel(sContext,pontuacao,vidas);
			desenho_mapa(sContext);
			desenho_pilulas(sContext,Pilulas);
			desenho_vitaminas(sContext,Vitaminas);
			jogo_normal = true;
			inicia_variaveis();
		}
		else 
		if(jogo_normal == false && fim_do_jogo == true){
		// Espera apertar botao para reiniciar o jogo
		// Mostra pontos!
		// Esperar apertar o botao
			escreve_frase_final(sContext,pontuacao);
			osTimerStop(frames_ID);
			osTimerStop(Troca_busca_guarda_ID);
			//Musica final
			musica_final();
			// Enquanto nao pressionar o botao
			while(button_read_s1() == false);
			osThreadTerminate(Menu_inicial_ID);
			Menu_inicial_ID = osThreadCreate (osThread(Menu_inicial), NULL);
			osThreadTerminate(Desenho_inicial_ID);
			Desenho_inicial_ID = osThreadCreate (osThread(Desenho_inicial), NULL);
			osThreadTerminate(Gerencia_botoes_ID);
			Gerencia_botoes_ID = osThreadCreate (osThread(Gerencia_botoes), NULL);
			osThreadTerminate(Pacman_ID);
			Pacman_ID = osThreadCreate (osThread(Pacman), NULL);
			osThreadTerminate(Fantasmas_ID);
			Fantasmas_ID = osThreadCreate (osThread(Fantasmas), NULL);
			osThreadTerminate(Gerencia_vitaminas_ID);
			Gerencia_vitaminas_ID = osThreadCreate (osThread(Gerencia_vitaminas), NULL);
			osThreadTerminate(Gerencia_pilulas_ID);
			Gerencia_pilulas_ID = osThreadCreate (osThread(Gerencia_pilulas), NULL);
			osThreadTerminate(Painel_instrumentos_ID);
			Painel_instrumentos_ID = osThreadCreate (osThread(Painel_instrumentos), NULL);
			reinicia_variaveis();
			// Marca o sinal do menu inicial
			osSignalSet(Menu_inicial_ID,0x01);
		}
		osThreadYield();
	}
}




// Cria��o dos timers
osTimerDef(frames,Timer_fps);
osTimerDef(Troca_busca_guarda,Timer_acao_fantasma);

int main() {
	
	// Inicia contador de ciclos
	#ifndef GANTT
	inicia_DWT();
	
	KIN1_InitCycleCounter(); /* enable DWT hardware */
	KIN1_ResetCycleCounter(); /* reset cycle counter */
	KIN1_EnableCycleCounter(); /* start counting */
	#endif
	
	// Inicia pilulas e vitaminas
	Inicia_pilulas(Pilulas);
	Inicia_vitamina(Vitaminas);
	
	// Inicializacao do timer
	frames_ID = osTimerCreate(osTimer(frames),osTimerPeriodic,(void *)0);
	Troca_busca_guarda_ID = osTimerCreate(osTimer(Troca_busca_guarda),osTimerPeriodic,(void *)0);
	
	buzzer_ID = osMutexCreate(osMutex(buzzer));
	check_vida_ID = osMutexCreate(osMutex(check_vida));
	
	// Inicializa o Kernel
	osKernelInitialize();	

#ifndef GANTT	
	//Initializing all peripherals
	init_all();

	// Inicializa o Display
	GrContextInit(&sContext, &g_sCfaf128x128x16);
	GrFlush(&sContext);
	GrContextFontSet(&sContext, &g_sFontCmss16b );
	GrContextBackgroundSet(&sContext, ClrBlack);


	//Seta volume do jogo
	buzzer_vol_set(5000);
#endif

	// Inicia variaveis dos personagens
	inicia_variaveis();

	// Inicializa as Threads
	if(Init_Thread()==-1)
		return 0;
		// Mensagens de erro de inicializa��o
	
	// Seta o signal do menu inicial
	osSignalSet(Menu_inicial_ID,0x01);
	
	// Inicializa o Kernel, junto com as threads
	osKernelStart();
	
	//Main aguarda para sempre
	osDelay(osWaitForever);

}
