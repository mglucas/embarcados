#include "PWM.h"

void PWM_function_init(void){

	uint32_t ui32Gen;

	// Variaveis para programar PWM (Tipo volateis: compilador nao elimina-as)
	volatile uint32_t ui32Load;
	volatile uint32_t ui32PWMClock;
	volatile uint8_t ui8Adjust;

	ui8Adjust = 83; //83: posicao central pra criar um pulso de 1.5mS do PWM.
	
	/*----------------------------
	--------- GPIO ---------------
	----------------------------*/
	
	// Enable the PWM0 peripheral
	SysCtlPeripheralEnable(SYSCTL_PERIPH_PWM0);
	while(!SysCtlPeripheralReady(SYSCTL_PERIPH_PWM0));
	
	// Wait for the PWM0 module to be ready
	SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOF);
	while(!SysCtlPeripheralReady(SYSCTL_PERIPH_GPIOF));
  
	// Configure PIN for use by the PWM peripheral
	GPIOPinTypePWM(GPIO_PORTF_BASE, GPIO_PIN_1 |GPIO_PIN_2);
	
	// Configures the alternate function of a GPIO pin
	// PF1_M0PWM1 --> piezo buzzer
	GPIOPinConfigure(GPIO_PF2_M0PWM2);
	
	/*----------------------------
	--------- PWM ---------------
	----------------------------*/
	
	//ui32PWMClock =  12000000 / 64;	//	divisor 64 roda o clock a 625kHz.
  //ui32Load = (ui32PWMClock / 50) - 1; // 50 Hertz
  PWMGenConfigure(PWM0_BASE, PWM_GEN_1, PWM_GEN_MODE_DOWN);
  PWMGenPeriodSet(PWM0_BASE, PWM_GEN_1, 37500);

  //PWMPulseWidthSet(PWM0_BASE, PWM_OUT_2, 35625);
  //PWMOutputState(PWM0_BASE, PWM_OUT_2_BIT, true);
	PWMGenEnable(PWM0_BASE, PWM_GEN_1);
	
	// Set the PWM period to 500Hz.  To calculate the appropriate parameter
	// use the following equation: N = (1 / f) * SysClk.  Where N is the
	// function parameter, f is the desired frequency, and SysClk is the
	// system clock frequency.
	// In this case you get: (1 / 500) * 120MHz / 64 = 3750 cycles.  Note that
	// the maximum period you can set is 2^16.
	// TODO: modify this calculation to use the clock frequency that you are
	// using.
	
	///buzzer_per_set(0xFFFF);
}
