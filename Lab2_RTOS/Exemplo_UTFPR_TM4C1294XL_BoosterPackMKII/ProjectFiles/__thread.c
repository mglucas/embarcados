#include "cmsis_os.h"
#include <stdio.h>
#include <stdlib.h>
#include "TM4C129.h"                    // Device header
#include <stdbool.h>
#include "grlib/grlib.h"
#include "cfaf128x128x16.h"
#include "led.h"
#include "driverlib/sysctl.h" // driverlib
#include <string.h>
 
#define TIMER 1000 
 
 // Mensagem 1
int mensagem1[300] = {5,10,9,9,0,1,0,0,6,6,6,7,15,14,15,15,7,6,9,9,0,1,0,0,6,8,6,7,15,14,15,15,7,11,9,9,0,1,0,0,6,1,6,7,15,14,15,15,2,7,9,9,0,1,0,0,2,6,6,7,15,14,15,15,2,7,9,9,0,1,0,0,4,12,6,7,15,14,15,15,6,8,9,9,0,1,0,0,6,7,6,7,15,14,15,15,7,11,9,9,0,1,0,0,5,10,6,7,15,14,15,15,7,5,9,9,0,1,0,0,5,10,6,7,15,14,15,15,2,7,9,9,0,1,0,0,5,15,6,7,15,14,15,15,6,12,9,9,0,1,0,0,5,10,6,7,15,14,15,15,7,11,9,9,0,1,0,0,2,7,6,7,15,14,15,15,2,7,9,9,0,1,0,0,4,11,6,7,15,14,15,15,7,6,9,9,0,1,0,0,5,11,6,7,15,14,15,15,2,7,9,9,0,1,0,0,4,13,6,7,15,14,15,15,6,15,9,9,0,1,0,0,6,8,6,7,15,14,15,15,7,4,9,9,0,1,0,0,5,10,6,7,15,14,15,15,7,10,9,9,0,1,0,0,8,10,6,5,0,2,0,0,8,14,11,15,15,14,15,15};
// Mensagem 2
int mensagem2[300] = {8,8,2,0,10,14,8,11,3,3,14,0,5,1,7,4,10,8,2,0,10,14,8,11,2,15,14,0,5,1,7,4,11,12,2,0,10,14,8,11,13,13,13,15,5,1,7,4,8,5,2,0,10,14,8,11,2,15,14,0,5,1,7,4,10,8,2,0,10,14,8,11,1,14,14,0,5,1,7,4,11,7,2,0,10,14,8,11,2,5,14,0,5,1,7,4,6,3,2,0,10,14,8,11,1,6,14,0,5,1,7,4,11,2,2,0,10,14,8,11,3,2,14,0,5,1,7,4,6,3,2,0,10,14,8,11,1,1,14,0,5,1,7,4,10,4,2,0,10,14,8,11,2,8,14,0,5,1,7,4,10,8,2,0,10,14,8,11,13,13,13,15,5,1,7,4,7,0,2,0,10,14,8,11,13,13,13,15,5,1,7,4,9,7,2,0,10,14,8,11,2,5,14,0,5,1,7,4,10,8,2,0,10,14,8,11,13,13,13,15,5,1,7,4,9,3,2,0,10,14,8,11,2,12,14,0,5,1,7,4,10,15,2,0,10,14,8,11,2,6,14,0,5,1,7,4,10,6,2,0,10,14,8,11,6,4,3,0,8,5,13,1,11,14,13,15,5,1,7,4};	
// Mensagem 3
int mensagem3[300] = {0,7,6,10,0,0,0,0,11,5,9,6,15,15,15,15,1,8,6,10,0,0,0,0,6,13,9,6,15,15,15,15,15,7,6,9,0,0,0,0,11,12,9,6,15,15,15,15,2,2,6,10,0,0,0,0,11,15,9,6,15,15,15,15,2,6,6,10,0,0,0,0,6,13,9,6,15,15,15,15,14,0,6,9,0,0,0,0,6,13,9,6,15,15,15,15,15,15,6,9,0,0,0,0,11,6,9,6,15,15,15,15,1,10,6,10,0,0,0,0,11,5,9,6,15,15,15,15,2,7,6,10,0,0,0,0,6,13,9,6,15,15,15,15,0,0,6,10,0,0,0,0,12,6,9,6,15,15,15,15,13,3,6,9,0,0,0,0,9,3,9,6,15,15,15,15,1,12,6,10,0,0,0,0,11,15,9,6,15,15,15,15,1,8,6,10,0,0,0,0,6,13,9,6,15,15,15,15,13,3,6,9,0,0,0,0,6,13,9,6,15,15,15,15,13,3,6,9,0,0,0,0,6,13,9,6,15,15,15,15,13,3,6,9,0,0,0,0,6,13,9,6,15,15,15,15,13,3,6,9,0,0,0,0,8,12,9,14,0,0,0,0,1,0,0,0,0,0,0,0};
// Mensagem 4
int mensagem[300] = {12,5,9,7,13,7,1,7,15,7,6,8,2,8,14,8,13,6,9,7,13,7,1,7,10,15,6,8,2,8,14,8,11,6,9,7,13,7,1,7,15,0,6,8,2,8,14,8,13,8,9,7,13,7,1,7,15,11,6,8,2,8,14,8,13,6,9,7,13,7,1,7,0,2,6,9,2,8,14,8,9,1,9,7,13,7,1,7,11,12,6,8,2,8,14,8,9,1,9,7,13,7,1,7,13,7,6,8,2,8,14,8,14,0,9,7,13,7,1,7,0,3,6,9,2,8,14,8,13,6,9,7,13,7,1,7,15,11,6,8,2,8,14,8,9,1,9,7,13,7,1,7,13,2,6,8,2,8,14,8,13,2,9,7,13,7,1,7,15,11,6,8,2,8,14,8,13,10,9,7,13,7,1,7,15,5,6,8,2,8,14,8,14,0,9,7,13,7,1,7,0,1,6,9,2,8,14,8,13,15,9,7,13,7,1,7,15,8,6,8,2,8,14,8,13,2,9,7,13,7,1,7,10,15,6,8,2,8,14,8,9,1,9,7,13,7,1,7,10,15,6,8,2,8,14,8,9,1,9,7,13,7,1,7,2,9,6,3,12,3,2,3,8,15,6,8,2,8,14,8};
// Mensagem 5
int mensagem5[300] = {12,13,4,15,4,6,10,13,14,7,11,0,11,9,5,2,0,0,5,0,4,6,10,13,13,8,11,0,11,9,5,2,15,0,4,15,4,6,10,13,9,5,11,0,11,9,5,2,13,14,4,15,4,6,10,13,14,5,11,0,11,9,5,2,15,13,4,15,4,6,10,13,13,14,11,0,11,9,5,2,15,9,4,15,4,6,10,13,13,12,11,0,11,9,5,2,15,14,4,15,4,6,10,13,14,9,11,0,11,9,5,2,15,0,4,15,4,6,10,13,13,10,11,0,11,9,5,2,15,9,4,15,4,6,10,13,9,5,11,0,11,9,5,2,11,8,4,15,4,6,10,13,9,5,11,0,11,9,5,2,12,13,4,15,4,6,10,13,14,4,11,0,11,9,5,2,15,13,4,15,4,6,10,13,14,3,11,0,11,9,5,2,10,11,4,15,4,6,10,13,14,9,11,0,11,9,5,2,15,10,4,15,4,6,10,13,9,5,11,0,11,9,5,2,13,13,4,15,4,6,10,13,14,10,11,0,11,9,5,2,15,9,4,15,4,6,10,13,9,5,11,0,11,9,5,2,10,11,4,15,4,6,10,13,5,0,7,7,14,9,0,3,7,5,11,0,11,9,5,2};

	
// Vari�veis globais
uint32_t cycles;
double timer;
uint32_t primos[100];
uint32_t mensagem_calculada[100];
int	mensagem_calculada_length=0;
char valor_char[50];
char valor_char2[50];
char arr[sizeof(timer)];
char chave_final_arr[10];
int task = 0; // Controle de atividades
int test_num = 0; // Teste do n�mero 
uint32_t chave; //Chave que est� sendo testada
uint32_t chave_final=0;
uint32_t primoAnterior; //vari�vel que guarda o primo anterior
uint32_t timer_fim,timer_ini;

// Fun��o inicia perif�ricos	
void init_all(){
	cfaf128x128x16Init();
	led_init();
}

// Verifica se o numero primo
int isPrime(uint32_t number){
		uint32_t i;
	
    if(number < 2) return 0;
    if(number == 2) return 1;
    if(number % 2 == 0) return 0;
    for(i=3; (i*i)<=number; i+=2){
        if(number % i == 0 ) return 0;
    }
    return 1;
}

// Calcula a mensagem
void calcula_mensagem(){
	int j=0;
	int i=0;
	
	for(i=0;i<50;i++){
		mensagem_calculada[i] = (mensagem[j]*16 + mensagem[j+1]*1 + mensagem[j+2]*16*16*16 + mensagem[j+3]*16*16 + mensagem[j+4]*16*16*16*16*16 + mensagem[j+5]*16*16*16*16 + mensagem[j+6]*16*16*16*16*16*16*16 + mensagem[j+7]*16*16*16*16*16*16);
		j+=8;
		if(mensagem_calculada[i]==0)
			break;
		mensagem_calculada_length++;
	}
}

/*----------------------------------------------------------------------------
 *  Transforming int to string
 *---------------------------------------------------------------------------*/
static void intToString(int64_t value, char * pBuf, uint32_t len, uint32_t base, uint8_t zeros){
	static const char* pAscii = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	bool n = false;
	int pos = 0, d = 0;
	int64_t tmpValue = value;

	// the buffer must not be null and at least have a length of 2 to handle one
	// digit and null-terminator
	if (pBuf == NULL || len < 2)
			return;

	// a valid base cannot be less than 2 or larger than 36
	// a base value of 2 means binary representation. A value of 1 would mean only zeros
	// a base larger than 36 can only be used if a larger alphabet were used.
	if (base < 2 || base > 36)
			return;

	if (zeros > len)
		return;
	
	// negative value
	if (value < 0)
	{
			tmpValue = -tmpValue;
			value    = -value;
			pBuf[pos++] = '-';
			n = true;
	}

	// calculate the required length of the buffer
	do {
			pos++;
			tmpValue /= base;
	} while(tmpValue > 0);


	if (pos > len)
			// the len parameter is invalid.
			return;

	if(zeros > pos){
		pBuf[zeros] = '\0';
		do{
			pBuf[d++ + (n ? 1 : 0)] = pAscii[0]; 
		}
		while(zeros > d + pos);
	}
	else
		pBuf[pos] = '\0';

	pos += d;
	do {
			pBuf[--pos] = pAscii[value % base];
			value /= base;
	} while(value > 0);
}

static void floatToString(float value, char *pBuf, uint32_t len, uint32_t base, uint8_t zeros, uint8_t precision){
	static const char* pAscii = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	uint8_t start = 0xFF;
	if(len < 2)
		return;
	
	if (base < 2 || base > 36)
		return;
	
	if(zeros + precision + 1 > len)
		return;
	
	intToString((int64_t) value, pBuf, len, base, zeros);
	while(pBuf[++start] != '\0' && start < len); 

	if(start + precision + 1 > len)
		return;
	
	pBuf[start+precision+1] = '\0';
	
	if(value < 0)
		value = -value;
	pBuf[start++] = '.';
	while(precision-- > 0){
		value -= (uint32_t) value;
		value *= (float) base;
		pBuf[start++] = pAscii[(uint32_t) value];
	}
}



// Vari�veis do display
tContext sContext;

// Flags de controle
int pen_fim = 0;
int ult_fim = 0;
int verificaChavePen = 0;
int verificaChaveUlt = 0;
int pen_original = 0;
int ult_original = 0;

//Guarda os valores originais
uint32_t pen_decodificada = 0;
uint32_t ult_decodificada = 0;
uint32_t ult_codificada = 0;
uint32_t pen_codificada = 0;
bool pen_par; //vari�vel que identifica se o pen�ltimo � par ou impar
bool ult_par;
bool imprimindo;
bool imprimir;

// Cria��o das threads 
void geradorPrimo(void const *argument);
void verificadorPrimo(void const *argument);
void decodificadorMsg(void const *argument);
void verificaPenultimo(void const *argument);
void verificaUltimo(void const *argument);
void mostraResultado(void const *argument);
void mostraTempo(void const *argument);

// Vari�vel que determina ID das threads
osThreadId geradorPrimo_ID; 
osThreadId verificadorPrimo_ID; 
osThreadId decodificadorMsg_ID; 
osThreadId verificaPenultimo_ID; 
osThreadId verificaUltimo_ID; 
osThreadId mostraResultado_ID; 
osThreadId mostraTempo_ID; 

// Dfini��o das threads
osThreadDef (geradorPrimo, osPriorityNormal, 1, 0);     // thread object
osThreadDef (verificadorPrimo, osPriorityNormal, 1, 0);     // thread object
osThreadDef (decodificadorMsg, osPriorityNormal, 1, 0);     // thread object
osThreadDef (verificaPenultimo, osPriorityNormal, 1, 0);     // thread object
osThreadDef (verificaUltimo, osPriorityNormal, 1, 0);     // thread object
osThreadDef (mostraResultado, osPriorityNormal, 1, 0);     // thread object
osThreadDef (mostraTempo, osPriorityNormal, 1, 0);     // thread object

// Inicializa as threads
int Init_Thread (void) {

	geradorPrimo_ID = osThreadCreate (osThread(geradorPrimo), NULL);
	if (!geradorPrimo_ID) return(-1);
	
	verificadorPrimo_ID = osThreadCreate (osThread(verificadorPrimo), NULL);
	if (!verificadorPrimo_ID) return(-1);
	
	decodificadorMsg_ID = osThreadCreate (osThread(decodificadorMsg), NULL);
	if (!decodificadorMsg_ID) return(-1);
	
	verificaPenultimo_ID = osThreadCreate (osThread(verificaPenultimo), NULL);
	if (!verificaPenultimo_ID) return(-1);
	
	verificaUltimo_ID = osThreadCreate (osThread(verificaUltimo), NULL); 
	if (!verificaUltimo_ID) return(-1);
	
	mostraResultado_ID = osThreadCreate (osThread(mostraResultado), NULL); 
	if (!mostraResultado_ID) return(-1);
	
	mostraTempo_ID = osThreadCreate (osThread(mostraTempo), NULL); 
	if (!mostraTempo_ID) return(-1);
	
  return(0);
}




// Thread que gear os n�meros  
void geradorPrimo (void const *argument) {
  uint32_t piso,teto;
			int j=0,i=0;
			uint32_t primo;
		
			mensagem_calculada_length =0;
			calcula_mensagem();
		
			// Calculo do piso e do teto
			piso = mensagem[j+2]*16*16*16 + mensagem[j+3]*16*16 + mensagem[j+4]*16*16*16*16*16 + mensagem[j+5]*16*16*16*16 + mensagem[j+6]*16*16*16*16*16*16*16 + mensagem[j+7]*16*16*16*16*16*16; 
			teto = piso + 255;
			
			while(teto >= piso-250){
				//Chave para ser testada eh a maior no intervalo encontrado
				primo = teto;
				if(isPrime(primo))
					primos[i++]=primo;
				teto--;
			}
			
	
			while (1) {
    if(task != 0)
			osThreadYield ();                             
    else{ 
			
			for(i=0;primos[i+2]!=0;i++){
				// Guarda na chave o primo a ser verificado
				chave = primos[i];
				//Guarda aqui o primo anterior para ser utilizado na verifica��o
				primoAnterior = primos[i+1];
				task = 1;
				osThreadYield();
				osDelay(TIMER);
				while(imprimindo)
					osThreadYield();
					
			}
		}			
	}	
}
// Thread que verifica os primos
void verificadorPrimo (void const *argument) {
  while (1) {
    if(task != 1 || (verificaChavePen == 1 && verificaChaveUlt == 1) )
			osThreadYield ();                             
    else{
			// Se for primo, task = 2 
			if(isPrime(chave))
				task = 2;
			// Se nao for, volta para a proxima alimentacao
			else
				task = 0;
			osThreadYield();
			osDelay(100);
			while(imprimindo)
				osThreadYield();
					
		}	
  }
}

// Thread que decodifica a mensagem
void decodificadorMsg(void const *argument){
	int i=0,j=0,l=1;
	int valor=0;
	
  while (1) {
    if(task != 2)
			osThreadYield ();                             
    else{
			//Guardar nessas vari�veis os valores da pen�ltima e �ltima posi��o decodificado
			pen_codificada = mensagem_calculada[mensagem_calculada_length-2];
			ult_codificada = mensagem_calculada[mensagem_calculada_length-1];
			
			//Especifica se o pen�ltimo � par ou impar: par - 2 ou impar - 1
			if((mensagem_calculada_length-1)%2 == 0){
				ult_par = true;
				pen_par = false;
			}
			else{
				ult_par = false;
				pen_par = true;
			}

			for(i=0;i<mensagem_calculada_length;i++){
				if(i%2){
					valor = mensagem_calculada[i]+chave;
					if(i<21)
						valor_char[i] = (char) valor;
					else	
						valor_char2[i-21] = (char) valor;
				}
				else{
					valor = mensagem_calculada[i]-chave;
					if(i<21)
						valor_char[i] = (char) valor;
					else	
						valor_char2[i-21] = (char) valor;
				}
				valor = 0;
			}
			
			task = 3;
			osThreadYield();
							osDelay(TIMER);
			while(imprimindo)
				osThreadYield();
						
			}
		}
}	
	
// Thread que verifica o penultimo valor
void verificaPenultimo(void const *argument){
  int verifica_pen=0;
	
	while (1) {
    if(task != 3 || pen_fim ==1)
			osThreadYield ();                             
    else{
			
			pen_decodificada = pen_codificada;
			
			//Realiza a opera��o � ser verificada
			if(pen_par == true)
				verifica_pen = (int)(chave/2) - chave;
			else
				verifica_pen = (int)(chave/2) + chave;
			
			//Verifica se a chave bate com o valor da pen�ltima
			if(pen_decodificada == verifica_pen){
				verificaChavePen = 1;
				chave_final = chave;
			}
			pen_fim = 1;	
			
			if(pen_fim==1 && ult_fim==1){
				pen_fim = 0;
				ult_fim = 0;
				task = 4;
				osThreadYield ();

			}
			else 
				task = 3;
			osThreadYield ();
							osDelay(TIMER);
			while(imprimindo)
				osThreadYield();
		}
			
  }
}

// Thread que verifica o ultimo valor
void verificaUltimo(void const *argument){

	while (1) {
    if(task != 3 || ult_fim == 1)
			osThreadYield ();                             
    else{

			uint32_t verifica_ult;			
			ult_decodificada = 0;
			
			if(ult_par==true){
				verifica_ult =  (chave*chave)/primoAnterior - chave;
			}
			else{
				verifica_ult =  (chave*chave)/primoAnterior + chave;
			}
			
			//Verifica se a chave bate com o valor da pen�ltima
			if(ult_codificada == verifica_ult){
				verificaChaveUlt = 1;
				chave_final = chave;
			}
			ult_fim = 1;
			
			if(pen_fim==1 && ult_fim==1){
				pen_fim = 0;
				ult_fim = 0;
				task = 4;
				osThreadYield ();				
			}
			else 
				task = 3;
			osThreadYield (); 
				osDelay(TIMER);			
			while(imprimindo)
				osThreadYield();
		}		
  }
}
// Thread que mostra o resultado no oled
void mostraResultado(void const *argument){
	int i=0,j=0,l=1;
  GrContextBackgroundSet(&sContext, ClrBlack);
	GrContextForegroundSet(&sContext, ClrWhite);

	while (1) {
    if(task != 4)
			osThreadYield ();
		else{
			imprimindo = true;
			// Mostra a mensagem no oled
			GrStringDraw(&sContext,valor_char,21,1,1,true);
			GrStringDraw(&sContext,valor_char2,mensagem_calculada_length-21-2,1,10,true);
			
			// transforma a chave em um vetor de char
			intToString(chave,chave_final_arr,10,10,1);

			// Mostra a chave no oled
			GrStringDraw(&sContext,"Chave:",10,1,55,true);
			GrStringDraw(&sContext,chave_final_arr,10,65,55,true);
			
			if(verificaChaveUlt==1 && verificaChavePen==1){
				intToString(chave,chave_final_arr,10,10,1);

					// Zerando o vetor de primos
					//for (i=0;i<100;i++)
					//primos[i] = 0;

				//verificaChaveUlt = 0;
				//verificaChavePen = 0;
				task = 5;
			}
			else{
				task=1;
			}

			imprimindo = false;			
			osThreadYield();
			
		}
  }
}

// Thread que finaliza e mostra o tempo de execucao
void mostraTempo(void const *argument){
			GrContextBackgroundSet(&sContext, ClrBlack);
		  GrContextForegroundSet(&sContext, ClrWhite);
  while (1) {
    if(task != 5)
			osThreadYield ();                             
    else{
			
			// Fim do tempo de contagem
			timer_fim = osKernelSysTick();
			
			// Delta de tempo
			timer = timer_fim - timer_ini;
			
			// Valor em ms
			timer = timer/100000;
			
			// Carrega o timer em um vetor de char
			floatToString(timer,arr,10,10,1,2);
			
			// Carrega a chave no array
			intToString(chave_final,chave_final_arr,10,10,1);

			// Fun��es que escrevem no oled
			GrStringDraw(&sContext,"Chave:",10,1,55,true);
			GrStringDraw(&sContext,chave_final_arr,10,65,55,true);
			GrStringDraw(&sContext,"Tempo(mS):",10,1,64,true);
			GrStringDraw(&sContext,arr,6,65,64,true);
			while(1){
			task=5;
			osDelay(osWaitForever);
			}
		}
			
  }
}


int main() {
	int i;
		
	//Initializing all peripherals
	init_all();
	
	// Inicializa o Kernel
	osKernelInitialize();
	
	// Inicializa o Display
	GrContextInit(&sContext, &g_sCfaf128x128x16);
	GrFlush(&sContext);
	GrContextFontSet(&sContext, g_psFontFixed6x8 );
	GrContextBackgroundSet(&sContext, ClrBlack);
	
	// Inicio do tempo de contagem
	timer_ini = osKernelSysTick();
	
	// Zerando o vetor para controle
	for (i=0;i<50;i++)
		mensagem_calculada[i]=0;
	
	// Zerando o vetor de char para controle
	for (i=0;i<50;i++)
		valor_char[i] = '\0';
	for (i=0;i<50;i++)
		valor_char2[i] = '\0';
	// Zerando o vetor de primos
	for (i=0;i<100;i++)
		primos[i] = 0;
		
	// Inicializa as Threads
	if(Init_Thread()==0)
		return 0;
		// Mensagens de erro de inicializa��o
	
	
	// Inicializa o Kernel, junto com as threads
	osKernelStart();
	
	//Main aguarda para sempre
	osDelay(osWaitForever);

}
