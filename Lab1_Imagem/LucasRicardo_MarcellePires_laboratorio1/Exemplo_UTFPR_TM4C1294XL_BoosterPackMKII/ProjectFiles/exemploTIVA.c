/*============================================================================
 *                    Exemplos de utiliza��o do Kit
 *              EK-TM4C1294XL + Educational BooterPack MKII 
 *---------------------------------------------------------------------------*
 *                    Prof. Andr� Schneider de Oliveira
 *            Universidade Tecnol�gica Federal do Paran� (UTFPR)
 *===========================================================================
 * Autores das bibliotecas:
 * 		Allan Patrick de Souza - <allansouza@alunos.utfpr.edu.br>
 * 		Guilherme Jacichen     - <jacichen@alunos.utfpr.edu.br>
 * 		Jessica Isoton Sampaio - <jessicasampaio@alunos.utfpr.edu.br>
 * 		Mariana Carri�o        - <mcarriao@alunos.utfpr.edu.br>
 *===========================================================================*/
#include "cmsis_os.h"
#include "TM4C129.h"  // Device header
#include <stdbool.h>
#include "grlib/grlib.h"


/*----------------------------------------------------------------------------
 * include libraries from drivers
 *----------------------------------------------------------------------------*/

#include "rgb.h"
#include "cfaf128x128x16.h"
#include "servo.h"
#include "temp.h"
#include "opt.h"
#include "joy.h"
#include "mic.h"
#include "accel.h"
#include "led.h"
#include "buttons.h"


#define LED_A      0
#define LED_B      1
#define LED_C      2
#define LED_D      3
#define LED_CLK    7

//To print on the screen
tContext sContext;

//PPM image
//extern const unsigned char MyBinaryImage1[];
//extern const unsigned long MyBinaryImage1_length; // this constant is calculated in the assembler source file


void init_all(){
	cfaf128x128x16Init();
	led_init();
	joy_init();
	button_init();
}
/*----------------------------------------------------------------------------
 *  Transforming int to string
 *---------------------------------------------------------------------------*/
static void intToString(int64_t value, char * pBuf, uint32_t len, uint32_t base, uint8_t zeros){
	static const char* pAscii = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	bool n = false;
	int pos = 0, d = 0;
	int64_t tmpValue = value;

	// the buffer must not be null and at least have a length of 2 to handle one
	// digit and null-terminator
	if (pBuf == NULL || len < 2)
			return;

	// a valid base cannot be less than 2 or larger than 36
	// a base value of 2 means binary representation. A value of 1 would mean only zeros
	// a base larger than 36 can only be used if a larger alphabet were used.
	if (base < 2 || base > 36)
			return;

	if (zeros > len)
		return;
	
	// negative value
	if (value < 0)
	{
			tmpValue = -tmpValue;
			value    = -value;
			pBuf[pos++] = '-';
			n = true;
	}

	// calculate the required length of the buffer
	do {
			pos++;
			tmpValue /= base;
	} while(tmpValue > 0);


	if (pos > len)
			// the len parameter is invalid.
			return;

	if(zeros > pos){
		pBuf[zeros] = '\0';
		do{
			pBuf[d++ + (n ? 1 : 0)] = pAscii[0]; 
		}
		while(zeros > d + pos);
	}
	else
		pBuf[pos] = '\0';

	pos += d;
	do {
			pBuf[--pos] = pAscii[value % base];
			value /= base;
	} while(value > 0);
}


/*----------------------------------------------------------------------------
 *      Main
 *---------------------------------------------------------------------------*/

extern uint8_t ReadImage1(void);
extern uint8_t ReadImage2(void);
extern void Start_asm(void);
extern void RightImage(void);
extern void LeftImage(void);
extern void UpImage(void);
extern void DownImage(void);

int main (void) {

	//Joystick
	int16_t joy_x=0,joy_y=0;
	char pbufx[10], pbufy[10];
	bool troca;
	int chamada=1, mem_chamada=1;
	int joy_direcao=0; //Se 1 -> cima e baixo, se 0 -> Laterais
	int button =0 ;

	uint8_t linha=0,coluna=0,cor=0;
	
	//Initializing all peripherals
	init_all();
	
	Start_asm();
	
	GrContextInit(&sContext, &g_sCfaf128x128x16);
	GrFlush(&sContext);
	GrContextFontSet(&sContext, g_psFontFixed6x8);
	GrContextBackgroundSet(&sContext, ClrBlack);
	
	while(1){
		for(linha=0;linha<64;linha++){
				for(coluna=0;coluna<96;coluna++){
					
					if(joy_direcao == 1)
						cor = ReadImage1();
					else if(joy_direcao == 0)
						cor = ReadImage2();
				
					if(troca){
						if (cor > 150)
							GrContextForegroundSet(&sContext, ClrWhite);
						else
							GrContextForegroundSet(&sContext, ClrBlack);
					}
					else
						if (cor > 150)
							GrContextForegroundSet(&sContext, ClrBlack);
						else
							GrContextForegroundSet(&sContext, ClrWhite);	
					
					GrPixelDraw(&sContext,coluna,linha);
					
					cor=0;
				}
				
			}
		

/* Bot�es */
			while(button_read_s1())
				button = 1;
			
			if(button){
				if(chamada==1){
					chamada = 5;
					mem_chamada = chamada;
				}
				else{
					chamada=1;
					mem_chamada = chamada;
				}
				
				button = 0;
			}
			
/*  Joystick		*/		
			if (joy_read_center())
				troca = !troca;
			
			joy_x = joy_read_x();
			joy_y = joy_read_y();
		
			joy_x = joy_x*200/0xFFF-100;
			joy_y = joy_y*200/0xFFF-100;
		
			// joystik cima
			if ( joy_y > 90 & joy_x > -20 & joy_x < 20){
				joy_direcao = 1;
				
				while(chamada){
					UpImage();
					chamada--;
				}
				
			}								
			else
			// joystick baixo
			if ( joy_y < -90 & joy_x > -20 & joy_x < 20){
				joy_direcao = 1;
				
				while(chamada){
					DownImage();
					chamada--;
				}
			}									
			else
			// joystick esquerda
			if ( joy_x < -90 & joy_y > -20 & joy_y < 20){
				joy_direcao = 0;
				
				while(chamada){
					LeftImage();
					chamada--;
				}

			}	
			else
			// joystick direita
			if ( joy_x > 80 & joy_y > -20 & joy_y < 20){
				joy_direcao = 0;
				
				while(chamada){
					RightImage();
					chamada--;
				}
			}									
			
		chamada = mem_chamada;
		Start_asm();
	}

	
}

