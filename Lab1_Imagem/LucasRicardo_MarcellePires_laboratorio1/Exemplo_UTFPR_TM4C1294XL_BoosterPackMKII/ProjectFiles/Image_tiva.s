		AREA    MyBinFile1_Section, DATA, READWRITE

		EXPORT  MyBinaryImage1
        EXPORT  MyBinaryImage1_length
		EXPORT  MyBinaryImage2
        EXPORT  MyBinaryImage2_length

;Imagem 1 -  Avi�o
MyBinaryImage1
		INCBIN airplane.ppm
MyBinaryImage1_End

;Imagem 2 - �nibus
MyBinaryImage2
		INCBIN bus.ppm
MyBinaryImage2_End
		ALIGN
			
; Comprimento das imagens			
MyBinaryImage1_length
        DCD     MyBinaryImage1_End - MyBinaryImage1				
MyBinaryImage2_length
        DCD     MyBinaryImage2_End - MyBinaryImage2
			
		END
        		
